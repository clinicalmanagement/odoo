FROM python:3.7-slim-bullseye
LABEL MAINTAINER "Pedro Andrés Ospina Salamanca <administracion@soysoham.com>"

LABEL org.opencontainers.image.vendor="Clinical" \
    org.opencontainers.image.authors="Pedro Andres Ospina Salamanca <administracion@soysoham.com>" \
    org.opencontainers.image.created="22-07-2022" \
    org.opencontainers.image.version="12.0" \
    org.opencontainers.image.version.odoo="12.0" \
    org.opencontainers.image.url="https://soysoham.com/" \
    org.opencontainers.image.documentation="https://gitlab.com/feelapp.cloud/odoo" \
    org.opencontainers.image.source="https://gitlab.com/feelapp.cloud/odoo" \
    org.opencontainers.image.source.odoo="https://github.com/odoo/odoo/tree/15.0" \
    org.opencontainers.image.ref.name="12.0" \
    org.opencontainers.image.title="feelapp.cloud 12.0" \
    org.opencontainers.image.description="An Docker base image for to build the Odoo \
    12.0 version that has the feelapp.cloud source code pre-built."

# Set debconf to run non-interactively
ARG DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-xo", "pipefail", "-c"]

# Generate locale C.UTF-8 for postgres and general locale data
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Copy dependecies files for install
COPY setup/package.debian.txt /opt/source/
COPY setup/package.libraries.txt /opt/source/
COPY setup/package.python3.txt /opt/source/
COPY setup/package.postgresql.txt /opt/source/
COPY setup/extra-requirements.txt /opt/source/
COPY setup/package.npm.txt /opt/source/

WORKDIR /opt/source/

RUN apt-get update -qq && \
    apt-get upgrade -qq -y && \
    apt-get -qq install apt-utils dialog && \
    apt-get -qq install keyboard-configuration

# Install some Odoo deps like python header development, libraries and wkhtmltopdf
RUN set -x; \
    apt-get update -qq \
    && apt-get install -yqq --no-install-recommends \
    $(grep -v '^#' package.debian.txt) \
    $(grep -v '^#' package.libraries.txt) \
    $(grep -v '^#' package.python3.txt) \
    && curl -o wkhtmltox.deb -sSL https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.buster_amd64.deb \
    && echo "ea8277df4297afc507c61122f3c349af142f31e5 wkhtmltox.deb" | sha1sum -c - \
    && dpkg --force-depends -i wkhtmltox.deb \
    && apt-get -yqq install -f --no-install-recommends \
    && rm -rf /opt/source/wkhtmltox.deb \
    && rm -rf /var/lib/apt/lists/* /tmp/* \
    && apt-get purge --auto-remove \
    && apt-get clean \
    && sync

# Install latest postgresql-client
RUN echo 'deb http://apt.postgresql.org/pub/repos/apt/ bullseye-pgdg main' > /etc/apt/sources.list.d/pgdg.list \
    && GNUPGHOME="$(mktemp -d)" \
    && export GNUPGHOME \
    && repokey='B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8' \
    && gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "${repokey}" \
    && gpg --batch --armor --export "${repokey}" > /etc/apt/trusted.gpg.d/pgdg.gpg.asc \
    && gpgconf --kill all \
    && rm -rf "$GNUPGHOME" \
    && apt-get update \
    && apt-get -y install --no-install-recommends $(grep -v '^#' package.postgresql.txt)\
    && rm -f /etc/apt/sources.list.d/pgdg.list \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge   --auto-remove \
    && apt-get clean \
    && sync

# NodeJS installation
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash - \
    && apt-get -y install nodejs \
    && rm -rf /var/lib/apt/lists/* \
    && sync

# Install Node.js libreries (on Debian buster)
RUN cat package.npm.txt | xargs npm install -g \
    && rm -rf ~/.npm /tmp/* \
    && sync

WORKDIR /opt/odoo/

# Copy entrypoint script and Odoo configuration file
COPY ./entrypoint.sh /
RUN adduser --system --quiet --shell=/bin/bash --no-create-home --gecos 'odoo' --group odoo

# Location structure for odoo production
ENV ODOO_SRC odoo
RUN mkdir -p /opt/odoo/extra-addons \
    && mkdir -p /opt/odoo/data \
    && mkdir -p /opt/odoo/config \
    && mkdir -p /opt/odoo/enterprise \
    && mkdir -p /opt/odoo/location \
    && mkdir -p /opt/odoo/themes \
    && mkdir -p /opt/odoo/log \
    && mkdir -p /opt/odoo/drive \
    && chown -R odoo /opt

COPY ./odoo.conf /opt/odoo/config/odoo.conf
RUN chown odoo /opt/odoo/config/odoo.conf

# Docker Mounted Volumes
VOLUME ["/opt/odoo/data","/opt/odoo/log","/opt/odoo/extra-addons","/var/lib/odoo","/opt/odoo/enterprise","/opt/odoo/location","/opt/odoo/drive","/opt/odoo/themes"]

# Install Odoo
ENV ODOO_VERSION 12.0
ENV DEPTH 1
ENV PYTHONOPTIMIZE=1
ARG PATHBASE=/opt/odoo

# Directory for persistent volume
ARG ODOO.CONF=${PATHBASE}/config

# Download Odoo from Github
RUN git clone https://github.com/odoo/odoo.git -b ${ODOO_VERSION} --depth ${DEPTH} ${PATHBASE}/odoo

# Python Library for module installation and system
RUN apt update \
    && apt-get upgrade -y \
    && pip3 install setuptools \
    && pip3 install --no-cache-dir -U pip \
    && pip3 install --no-cache-dir -r /opt/odoo/odoo/requirements.txt \
    && pip3 install --no-cache-dir -r /opt/source/extra-requirements.txt \
    && (python3 -m compileall -q /usr/local/lib/python3.7/ || true) \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge --auto-remove \
    && apt-get clean \
    && sync

# Fix broken installation and remove/purge packages and config files unnecessary
RUN apt --fix-broken install \
    && apt-get -y install -f \
    && apt-get purge --auto-remove \
    && apt-get clean \
    && sync

# Expose Odoo Port Services
EXPOSE 8069 8071 8072

# Set the default config file
ENV ODOO_RC /opt/odoo/config/odoo.conf

# Copy wait-for-psql.py script file
COPY wait-for-psql.py /usr/local/bin/wait-for-psql.py

# Set default user when running the container
RUN ln -s /opt/odoo/odoo/odoo-bin /usr/bin/odoo \
    && chown -R odoo: /var/lib/odoo \
    && chown -R odoo: /usr/local/bin/ \
    && chown -R odoo: /opt

# Set default user when running the container
USER odoo

# CMD /usr/bin/odoo
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
CMD ["odoo"]